import { Cart } from './schema';
import { ItemData } from './types';

interface FindOne {
  items: ItemData[];
}

export const findOne = async (user: string): Promise<FindOne> => {
  const cart = await Cart.findOne({ user }).exec();
  if (cart === null) {
    return { items: [] };
  }

  return { items: cart.items };
};

interface Update {
  items: ItemData[];
}

export const update = async (user: string, items: ItemData[]): Promise<Update> => {
  const cart = await Cart.findOneAndUpdate({ user }, { user, items }).exec();
  if (cart === null) {
    await Cart.create({ user, items });
  }

  return { items };
};
