import { Document, Schema, Model, model } from 'mongoose';
import { ItemData } from './types';

export interface CartDocument extends Document {
  user: string;
  items: ItemData[];
}

export interface CartModel extends Model<CartDocument> {}

const cartSchema = new Schema<CartDocument, CartModel>({
  user: { type: String, required: true },
  items: [{ _id: { type: String, required: true }, amount: { type: Number, default: 1, required: true } }],
});

export const Cart = model<CartDocument, CartModel>('Cart', cartSchema);
