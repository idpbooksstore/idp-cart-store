import { findOne, update } from './model';
import { ItemData } from './types';

export const getItems = async (user: string) => findOne(user);

export const storeItem = async (user: string, { _id, amount }: ItemData) => {
  const { items } = await findOne(user);

  const newItems = items.some((item) => item._id === _id)
    ? items.map((item) => (item._id === _id ? { _id: item._id, amount: item.amount + amount } : item))
    : [...items, { _id, amount }];

  return await update(user, newItems);
};

export const deleteItem = async (user: string, { _id, amount }: ItemData) => {
  const { items } = await findOne(user);

  const newItems = items
    .map((item) => (item._id === _id ? { _id: item._id, amount: item.amount - amount } : item))
    .filter((item) => item.amount > 0);

  return await update(user, newItems);
};

export const clearItems = async (user: string) => {
  return await update(user, []);
};
