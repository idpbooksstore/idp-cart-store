import mongoose from 'mongoose';
import { router } from './controller';
import express from 'express';
import { env } from './env';

const PORT = env.server.port;

const MONGO_HOST = env.mongo.host;
const MONGO_DATABASE = env.mongo.database;
const MONGO_USERNAME = env.mongo.username;
const MONGO_PASSWORD = env.mongo.password;
const MONGO_URI = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOST}/${MONGO_DATABASE}`;

mongoose.connect(MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  authSource: 'admin',
});

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/cart', router);

app.listen(PORT);
