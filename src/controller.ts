import { clearItems, deleteItem, getItems, storeItem } from './service';
import Router, { Request, Response } from 'express';
import { validateItemData, validateUser } from './validate';
import { ItemData } from './types';

export const router = Router();

interface PostData {
  user: string;
  item: ItemData;
}

export const postCart = async (req: Request, res: Response) => {
  if (!validateItemData(req.body)) return res.status(400).send({ message: validateItemData.errors, items: [] });
  try {
    const { user, item } = req.body as PostData;
    const { items } = await storeItem(user, item);

    return res.status(200).send({ message: 'Success', items });
  } catch (error) {
    return res.status(500).send({ message: error, items: [] });
  }
};

export const getCart = async (req: Request, res: Response) => {
  if (!validateUser(req.params)) return res.status(400).send({ message: validateUser.errors, items: [] });
  try {
    const { user } = req.params;
    const { items } = await getItems(user);

    return res.status(200).send({ message: 'Success', items });
  } catch (error) {
    return res.status(500).send({ message: error, items: [] });
  }
};

interface DeleteData {
  user: string;
  item: ItemData;
}

export const deleteCart = async (req: Request, res: Response) => {
  if (!validateItemData(req.body)) return res.status(400).send({ message: validateItemData.errors, items: [] });
  try {
    const { user, item } = req.body as DeleteData;
    const { items } = await deleteItem(user, item);

    return res.status(200).send({ message: 'Success', items });
  } catch (error) {
    return res.status(500).send({ message: error, items: [] });
  }
};

export const clearCart = async (req: Request, res: Response) => {
  if (!validateUser(req.params)) return res.status(400).send({ message: validateUser.errors, items: [] });
  try {
    const { user } = req.params;
    const { items } = await clearItems(user);

    return res.status(200).send({ message: 'Success', items });
  } catch (error) {
    return res.status(500).send({ message: error, items: [] });
  }
};

router.get('/store/:user', getCart);
router.post('/add', postCart);
router.post('/rm', deleteCart);
router.delete('/store/:user', clearCart);
