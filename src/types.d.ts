export interface ItemData {
  _id: string;
  amount: number;
}
