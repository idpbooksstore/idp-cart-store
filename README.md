# IDP Cart Store Service

This module is used to save the state of the shopping cart.

It uses the route `POST /api/cart/store` to receive the user and the item data, identifier and amount, to store. The body of the request has to be a json with the following format: `{user: string, item: {_id: string, amount: int}}`. The `amount` must be greater than zero.

It uses the route `GET /api/cart/store/:user` to receive the user and respond with the list of items this user has added to his cart.
The `user` must be a string.

It uses the route `DELETE /api/cart/store` to receive the user and the item data, identifier and amount to delete. The body of the request has to be a json with the following format: `{user: string, item: {_id: string, amount: int}}`. The `amount` must be greater than zero.

It uses the route `DELETE /api/cart/store/:user` to receive the user that will have his cart cleared. The `user` must be a string.

The module responds with an object of type `{status: int, message: string, items: ItemData[]}`, where `ItemData` is an object containing the item `id` and quantity, `amount`. Codes for status can be found [here](./src/status.ts). In case of an error the message string can contain some useful information.

Response statusCode:
- `200` when the data is valid and there are no errors
- `400` if the data is invalid
- `500` if there are errors at runtime

## Routes

- `GET /api/cart/:user`
  - Fetch the shopping cart
  - Output:
  ```typescript
  {
    status: StatusCode, 
    message: string, 
    items: [{_id: string, amount: number}]
  }
  ```

- `POST /api/cart/add`
  - Save a production in the shopping cart
  - Body:
  ```typescript
  {user: string, item: {_id: string, amount: number}}
  ```
  - Output:
  ```typescript
  {
    status: StatusCode, 
    message: string, 
    items: [{_id: string, amount: number}]
  }
  ```

- `POST /api/cart/rm`
  - Delete a product from the shopping cart
  - Body:
  ```typescript
  {user: string, item: {_id: string, amount: number}}
  ```
  - Output:
  ```typescript
  {
    status: StatusCode, 
    message: string, 
    items: [{_id: string, amount: number}]
  }
  ```

- `DELETE /api/cart/:user`
  - Clear the shopping cart
  - Output:
  ```typescript
  {
    status: StatusCode, 
    message: string, 
    items: [{_id: string, amount: number}]
  }
  ```
## Docker Container

This module can be built as a docker container.

#### Environment Variables

- `PORT` - The port the server will listen on. Default to `8080`
- `HOST` - Default to `localhost`
- `NODE_ENV` - `production` or `development`

## Available Scripts

In the project directory, you can run:

### `npm run build`

First it runs the linter and the formatter.<br />
Builds the app for production to the `dist` folder.<br />

### `npm run start`

Runs the build from `dist`.<br />

### `npm run test`

Launches the test runner.<br />

### `npm run format`

Runs the formatter.<br />

### `npm run lint`

Runs the linter.<br />

### `npm run prebuild`

Runs the linter and the formatter.<br />

### `npm run build:live`

Runs the code in watch mode using nodemon and ts-node.<br />
