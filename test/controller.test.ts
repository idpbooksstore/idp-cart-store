jest.mock('../src/service');

import { clearCart, deleteCart, getCart, postCart } from '../src/controller';
import { clearItems, deleteItem, getItems, storeItem } from '../src/service';
import httpMocks from 'node-mocks-http';
import { mockFunction } from './util';
import { ItemData } from '../src/types';

describe('Cart Store Controller', () => {
  describe('Get Cart', () => {
    test('When the user parameter is negative, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'GET',
        url: '/api/cart/',
        params: {
          user: -1,
        },
        body: {},
      });
      const response = httpMocks.createResponse();

      await getCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the user parameter is NaN, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'GET',
        url: '/api/cart/',
        params: {
          user: 123,
        },
        body: {},
      });
      const response = httpMocks.createResponse();

      await getCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the user parameter is valid, then the statusCode is 200', async () => {
      const mockedGetItems = mockFunction(getItems);
      mockedGetItems.mockImplementation(async (user: string) => {
        return { items: [] };
      });
      const request = httpMocks.createRequest({
        method: 'GET',
        url: '/api/cart/',
        params: {
          user: '0fe',
        },
        body: {},
      });
      const response = httpMocks.createResponse();

      await getCart(request, response);

      expect(response.statusCode).toEqual(200);
    });
    test('When the user parameter is valid and getItems throws an error, then the statusCode is 500', async () => {
      const mockedGetItems = mockFunction(getItems);
      mockedGetItems.mockImplementation((user: string) => {
        throw new Error();
      });
      const request = httpMocks.createRequest({
        method: 'GET',
        url: '/api/cart/',
        params: {
          user: '0fe',
        },
        body: {},
      });
      const response = httpMocks.createResponse();

      await getCart(request, response);

      expect(response.statusCode).toEqual(500);
    });
  });
  describe('Post Cart', () => {
    test('When the body is empy, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/add',
        body: {},
      });
      const response = httpMocks.createResponse();

      await postCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the user is negative, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/',
        body: {
          user: -1,
          item: {
            _id: 0,
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await postCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the user is NaN, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/add',
        body: {
          user: 123,
          item: {
            _id: 0,
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await postCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the item is negative, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/add',
        body: {
          user: '0fe',
          item: {
            _id: -1,
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await postCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the item is NaN, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/add',
        body: {
          user: '0fe',
          item: {
            _id: 123,
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await postCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the amount is negative, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/add',
        body: {
          user: '0fe',
          item: {
            _id: '0fe',
            amount: -1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await postCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the amount is NaN, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/add',
        body: {
          user: '0fe',
          item: {
            _id: '0fe',
            amount: 'asd',
          },
        },
      });
      const response = httpMocks.createResponse();

      await postCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the body is valid, then the statusCode is 200', async () => {
      const mockedStoreItem = mockFunction(storeItem);
      mockedStoreItem.mockImplementation(async (user: string, item: ItemData) => {
        return { items: [{ _id: '0fe', amount: 1 }] };
      });
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/add',
        body: {
          user: '0fe',
          item: {
            _id: '0fe',
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await postCart(request, response);

      expect(response.statusCode).toEqual(200);
    });
    test('When the body is valid and storeItems throws an error, then the statusCode is 500', async () => {
      const mockedStoreItem = mockFunction(storeItem);
      mockedStoreItem.mockImplementation((user: string, item: ItemData) => {
        throw new Error();
      });
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/add',
        body: {
          user: '0fe',
          item: {
            _id: '0fe',
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await postCart(request, response);

      expect(response.statusCode).toEqual(500);
    });
  });
  describe('Delete Cart', () => {
    test('When the body is empy, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/rm',
        body: {},
      });
      const response = httpMocks.createResponse();

      await deleteCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the user is negative, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/rm',
        body: {
          user: -1,
          item: {
            _id: '0fe',
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await deleteCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the user is NaN, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/rm',
        body: {
          user: 123,
          item: {
            _id: '0fe',
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await deleteCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the item is negative, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/rm',
        body: {
          user: '0fe',
          item: {
            _id: -1,
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await deleteCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the item is NaN, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/rm',
        body: {
          user: '0fe',
          item: {
            _id: 123,
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await deleteCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the amount is negative, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/rm',
        body: {
          user: '0fe',
          item: {
            _id: '0fe',
            amount: -1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await deleteCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the amount is NaN, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/rm',
        body: {
          user: '0fe',
          item: {
            _id: '0fe',
            amount: 'asd',
          },
        },
      });
      const response = httpMocks.createResponse();

      await deleteCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the body is valid, then the statusCode is 200', async () => {
      const mockedDeleteItem = mockFunction(deleteItem);
      mockedDeleteItem.mockImplementation(async (user: string, item: ItemData) => {
        return { items: [{ _id: '0fe', amount: 0 }] };
      });
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/rm',
        body: {
          user: '0fe',
          item: {
            _id: '0fe',
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await deleteCart(request, response);

      expect(response.statusCode).toEqual(200);
    });
    test('When the body is valid and storeItems throws an error, then the statusCode is 500', async () => {
      const mockedDeleteItem = mockFunction(deleteItem);
      mockedDeleteItem.mockImplementation((user: string, item: ItemData) => {
        throw new Error();
      });
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/api/cart/rm',
        body: {
          user: '0fe',
          item: {
            _id: '0fe',
            amount: 1,
          },
        },
      });
      const response = httpMocks.createResponse();

      await deleteCart(request, response);

      expect(response.statusCode).toEqual(500);
    });
  });
  describe('Delete All Cart', () => {
    test('When the user parameter is negative, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'DELETE',
        url: '/api/cart/',
        params: {
          user: -1,
        },
        body: {},
      });
      const response = httpMocks.createResponse();

      await clearCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the user parameter is NaN, then the statusCode is 400', async () => {
      const request = httpMocks.createRequest({
        method: 'DELETE',
        url: '/api/cart/',
        params: {
          user: 123,
        },
        body: {},
      });
      const response = httpMocks.createResponse();

      await clearCart(request, response);

      expect(response.statusCode).toEqual(400);
    });
    test('When the user parameter is valid, then the statusCode is 200', async () => {
      const mockedClearItems = mockFunction(clearItems);
      mockedClearItems.mockImplementation(async (user: string) => {
        return { items: [] };
      });
      const request = httpMocks.createRequest({
        method: 'DELETE',
        url: '/api/cart/',
        params: {
          user: '0fe',
        },
        body: {},
      });
      const response = httpMocks.createResponse();

      await clearCart(request, response);

      expect(response.statusCode).toEqual(200);
    });
    test('When the user parameter is valid and getItems throws an error, then the statusCode is 500', async () => {
      const mockedClearItems = mockFunction(clearItems);
      mockedClearItems.mockImplementation((user: string) => {
        throw new Error();
      });
      const request = httpMocks.createRequest({
        method: 'DELETE',
        url: '/api/cart/',
        params: {
          user: '0fe',
        },
        body: {},
      });
      const response = httpMocks.createResponse();

      await clearCart(request, response);

      expect(response.statusCode).toEqual(500);
    });
  });
});
