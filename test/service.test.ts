jest.mock('../src/model');

import { clearItems, deleteItem, getItems, storeItem } from '../src/service';
import { findOne, update } from '../src/model';
import { mockFunction } from './util';
import { ItemData } from '../src/types';

describe('Cart Store Service', () => {
  describe('Store Item', () => {
    test('When the user is not stored, then the status is Success and the item is added to the cart', async () => {
      const mockedGet = mockFunction(findOne);
      mockedGet.mockImplementation(async (user: string) => {
        return { items: [] };
      });
      const mockedUpdate = mockFunction(update);
      mockedUpdate.mockImplementation(async (user: string, items: ItemData[]) => {
        return { items };
      });

      const result = await storeItem('0fe', { _id: '0fe', amount: 1 });

      expect(result.items).toEqual([{ _id: '0fe', amount: 1 }]);
    });
    test('When the user is stored and has this item in the cart, then the status is Success and the item is added to the cart with more amount', async () => {
      const mockedGet = mockFunction(findOne);
      mockedGet.mockImplementation(async (user: string) => {
        return { items: [{ _id: '0fe', amount: 1 }] };
      });
      const mockedUpdate = mockFunction(update);
      mockedUpdate.mockImplementation(async (user: string, items: ItemData[]) => {
        return { items };
      });

      const result = await storeItem('0fe', { _id: '0fe', amount: 1 });

      expect(result.items).toEqual([{ _id: '0fe', amount: 2 }]);
    });
    test('When the user is stored and does not have this item in the cart, then the status is Success and the item is added to the cart', async () => {
      const mockedGet = mockFunction(findOne);
      mockedGet.mockImplementation(async (user: string) => {
        return { items: [{ _id: '0fe', amount: 1 }] };
      });
      const mockedUpdate = mockFunction(update);
      mockedUpdate.mockImplementation(async (user: string, items: ItemData[]) => {
        return { items };
      });

      const result = await storeItem('0fe', { _id: '1fe', amount: 1 });

      expect(result.items).toEqual([
        { _id: '0fe', amount: 1 },
        { _id: '1fe', amount: 1 },
      ]);
    });
  });
  describe('Get Items', () => {
    test('When the user is not stored, then the status is Success and the item list is empty', async () => {
      const mockedGet = mockFunction(findOne);
      mockedGet.mockImplementation(async (user: string) => {
        return { items: [] };
      });

      const result = await getItems('0fe');

      expect(result.items).toEqual([]);
    });
    test('When the user is stored and has no items, then the status is Success and the item list contains no items', async () => {
      const mockedGet = mockFunction(findOne);
      mockedGet.mockImplementation(async (user: string) => {
        return { items: [] };
      });

      const result = await getItems('0fe');

      expect(result.items).toEqual([]);
    });
    test('When the user is stored, then the status is Success and the item list contains all the items', async () => {
      const mockedGet = mockFunction(findOne);
      mockedGet.mockImplementation(async (user: string) => {
        return {
          items: [
            { _id: '0fe', amount: 1 },
            { _id: '1fe', amount: 1 },
          ],
        };
      });

      const result = await getItems('0fe');

      expect(result.items).toEqual([
        { _id: '0fe', amount: 1 },
        { _id: '1fe', amount: 1 },
      ]);
    });
  });
  describe('Delete Item', () => {
    test('When the user is not stored, then the status is Success and the list is empty', async () => {
      const mockedGet = mockFunction(findOne);
      mockedGet.mockImplementation(async (user: string) => {
        return { items: [] };
      });
      const mockedUpdate = mockFunction(update);
      mockedUpdate.mockImplementation(async (user: string, items: ItemData[]) => {
        return { items };
      });

      const result = await deleteItem('0fe', { _id: '0fe', amount: 1 });

      expect(result.items).toEqual([]);
    });
    test('When the user is stored and has no items, then the status is Success and the list is empty', async () => {
      const mockedGet = mockFunction(findOne);
      mockedGet.mockImplementation(async (user: string) => {
        return { items: [] };
      });
      const mockedUpdate = mockFunction(update);
      mockedUpdate.mockImplementation(async (user: string, items: ItemData[]) => {
        return { items };
      });

      const result = await deleteItem('0fe', { _id: '0fe', amount: 1 });

      expect(result.items).toEqual([]);
    });
    test('When the user is stored and has items, then the status is Success and the list contains the same items minus the amount deleted', async () => {
      const mockedGet = mockFunction(findOne);
      mockedGet.mockImplementation(async (user: string) => {
        return {
          items: [
            { _id: '0fe', amount: 5 },
            { _id: '1fe', amount: 1 },
          ],
        };
      });
      const mockedUpdate = mockFunction(update);
      mockedUpdate.mockImplementation(async (user: string, items: ItemData[]) => {
        return { items };
      });

      const result = await deleteItem('0fe', { _id: '0fe', amount: 1 });

      expect(result.items).toEqual([
        { _id: '0fe', amount: 4 },
        { _id: '1fe', amount: 1 },
      ]);
    });
    test('When the user is stored and has items and the item is less than the amount deleted, then the status is Success and the list contains the same items minus the item deleted', async () => {
      const mockedGet = mockFunction(findOne);
      mockedGet.mockImplementation(async (user: string) => {
        return {
          items: [
            { _id: '0fe', amount: 1 },
            { _id: '1fe', amount: 1 },
          ],
        };
      });
      const mockedUpdate = mockFunction(update);
      mockedUpdate.mockImplementation(async (user: string, items: ItemData[]) => {
        return { items };
      });

      const result = await deleteItem('0fe', { _id: '0fe', amount: 3 });

      expect(result.items).toEqual([{ _id: '1fe', amount: 1 }]);
    });
  });
  describe('Clear Items', () => {
    test('When the user is not stored, then the status is Success and the list is empty', async () => {
      const mockedUpdate = mockFunction(update);
      mockedUpdate.mockImplementation(async (user: string, items: ItemData[]) => {
        return { items };
      });

      const result = await clearItems('0fe');

      expect(result.items).toEqual([]);
    });
    test('When the user is stored, then the status is Success and the list is empty', async () => {
      const mockedUpdate = mockFunction(update);
      mockedUpdate.mockImplementation(async (user: string, items: ItemData[]) => {
        return { items };
      });

      const result = await clearItems('0fe');

      expect(result.items).toEqual([]);
    });
  });
});
